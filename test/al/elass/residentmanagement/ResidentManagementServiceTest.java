/**
 * ResidentManagementServiceTest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */
package al.elass.residentmanagement;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.axis2.AxisFault;

import al.elass.residentmanagement.ResidentManagementServiceStub;
import al.elass.residentmanagement.ResidentManagementServiceStub.*;

/*
 *  ResidentManagementServiceTest Junit test case
 */

public class ResidentManagementServiceTest extends junit.framework.TestCase {
	
	@SuppressWarnings("serial")
	private static Map<String, Object> existingResident = new HashMap<String, Object>(){{
		try {
			put("name", "Resident #1");
			put("movedIn", new SimpleDateFormat("yyyy-MM-dd").parse("2010-09-01"));
			put("movedOut", new SimpleDateFormat("yyyy-MM-dd").parse("2017-08-31"));
			put("roomNumber", 3);
			put("info", "Arbitrary resident information.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}};
	@SuppressWarnings("serial")
	private static Map<String, Object> nonExistingResident = new HashMap<String, Object>(){{
		put("name", "Non-existing resident");
		put("roomNumber", -1);
		put("info", "Arbitrary resident information.");
	}};

	/**
	 * Auto generated test method
	 */
	public void testgetResidents() throws Exception {

		ResidentManagementServiceStub stub = new ResidentManagementServiceStub();
		GetResidents getResidents24 = (ResidentManagementServiceStub.GetResidents) getTestObject(ResidentManagementServiceStub.GetResidents.class);
		
		// Get a list of residents with a length larger than 0.
		GetResidentsResponse response = stub.getResidents(getResidents24);
		assertNotNull(response);
		assertNotNull(response.getResidentList());
		assertNotNull(response.getResidentList().getResident());
		assertTrue(response.getResidentList().getResident().length > 0);
	}

	/**
	 * Auto generated test method
	 */
	public void testupdateInfo() throws Exception {

		ResidentManagementServiceStub stub = new ResidentManagementServiceStub();
		UpdateInfo updateInfo26 = (UpdateInfo) getTestObject(UpdateInfo.class);

		Info info = new Info();
		info.setInfo("updated info " + randomString(4));
		UpdateInfoResponse response;

		// Test 1: Update info of an existing resident.
		updateInfo26.setInfo(info);
		updateInfo26.setResidentName((String) existingResident.get("name"));
		response = stub.updateInfo(updateInfo26);
		assertEquals((String) existingResident.get("name"), 
				response.getResident().getName());
		assertEquals(info.getInfo(),
				response.getResident().getInfo().getInfo());
		
		// Test 2: Update info of a non-existing resident.
		updateInfo26.setInfo(info);
		updateInfo26.setResidentName((String) nonExistingResident.get("name"));
		try {
			stub.updateInfo(updateInfo26);
			fail("updated info of a non-existing resident");
		} catch (NoSuchResident|AxisFault e) {}
	}

	/**
	 * Auto generated test method
	 */
	public void testmoveOutResident() throws Exception {

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date[] dates = {
			df.parse("2001-01-01"), df.parse("2002-02-02"), df.parse("2003-03-03"), 
			df.parse("2004-04-04"), df.parse("2005-05-05"), df.parse("2006-06-06"), 
			df.parse("2007-07-07"), df.parse("2008-08-08"), df.parse("2009-09-09"), 
			df.parse("2010-10-10"), df.parse("2011-11-11"), df.parse("2012-12-12")
		};
		Date date = dates[randomInt(0, dates.length - 1)];
		
		ResidentManagementServiceStub stub = new ResidentManagementServiceStub();
		MoveOutResident moveOutResident28 = (MoveOutResident) getTestObject(MoveOutResident.class);
		
		// Test 1: Move out an existing resident.
		moveOutResident28.setMoveOutDate(date);
		moveOutResident28.setResidentName((String) existingResident.get("name"));
		MoveOutResidentResponse response = stub.moveOutResident(moveOutResident28);
		assertEquals(date, response.getResident().getMovedOut());
		
		GetResident getResident = new GetResident();
		getResident.setResidentName((String) existingResident.get("name"));
		assertEquals(date, stub.getResident(getResident).getResident().getMovedOut());
		
		// Test 2: Move out a non-existing resident.
		moveOutResident28.setMoveOutDate(date);
		moveOutResident28.setResidentName((String) nonExistingResident.get("name"));
		try {
			stub.moveOutResident(moveOutResident28);
			fail("a non-existing resident was moved out");
		} catch (NoSuchResident|AxisFault e) {}
	}

	/**
	 * Auto generated test method
	 */
	public void testaddResident() throws Exception {

		ResidentManagementServiceStub stub = new ResidentManagementServiceStub();
		AddResident addResident30 = (AddResident) getTestObject(AddResident.class);

		Resident resident;
		
		// Test 1: Add a resident with a random new name.
		int countBefore = countResidents();
		
		resident = new Resident();
		resident.setName((String) existingResident.get("name") + randomString(5));
		resident.setMovedIn((Date) existingResident.get("movedIn"));
		resident.setMovedOut((Date) existingResident.get("movedOut"));
		resident.setRoomNumber((int) existingResident.get("roomNumber"));
		Info info = new Info();
		info.setInfo((String) existingResident.get("info") + randomString(10));
		resident.setInfo(info);
		addResident30.setResident(resident);
		AddResidentResponse response = stub.addResident(addResident30);
		// Test 1: Check if the returned resident is the resident that should've been added.
		assertEquals(resident.getName(), response.getResident().getName());
		assertEquals(resident.getMovedIn(), response.getResident().getMovedIn());
		assertEquals(resident.getMovedOut(), response.getResident().getMovedOut());
		assertEquals(resident.getRoomNumber(), response.getResident().getRoomNumber());
		assertEquals(info.getInfo(), response.getResident().getInfo().getInfo());
		// Test 1: Check if the total amount of residents has been increased by 1.
		int countAfter = countResidents();
		assertEquals(countBefore + 1, countAfter);
		
		// Test 1: Check if the resident was added correctly with all its values.
		GetResident getResident = new GetResident();
		getResident.setResidentName(resident.getName());
		GetResidentResponse grResponse = stub.getResident(getResident);
		assertEquals(resident.getName(), grResponse.getResident().getName());
		assertEquals(resident.getMovedIn(), grResponse.getResident().getMovedIn());
		assertEquals(resident.getMovedOut(), grResponse.getResident().getMovedOut());
		assertEquals(resident.getRoomNumber(), grResponse.getResident().getRoomNumber());
		assertEquals(info.getInfo(), grResponse.getResident().getInfo().getInfo());
		
		// Test 2: Add a resident that already exists.
		resident = new Resident();
		resident.setName((String) existingResident.get("name"));
		resident.setMovedIn((Date) existingResident.get("movedIn"));
		resident.setRoomNumber((int) existingResident.get("roomNumber"));
		addResident30.setResident(resident);
		try {
			stub.addResident(addResident30);
			fail("already existing resident added (" + resident.getName() + ")");
		} catch (ResidentAlreadyExists|AxisFault e) {}
	}

	/**
	 * Auto generated test method
	 */
	public void testisResident() throws Exception {
		ResidentManagementServiceStub stub = new ResidentManagementServiceStub();
		IsResident isResident = (IsResident) getTestObject(IsResident.class);
		IsResidentResponse response;
		
		// Test 1: check if existing entity exists.
		isResident.setResidentName((String) existingResident.get("name"));
		response = stub.isResident(isResident);
		assertTrue(response.getIsResident());
		
		// Test 2: check if non-existing entity exists.
		isResident.setResidentName((String) nonExistingResident.get("name"));
		response = stub.isResident(isResident);
		assertFalse(response.getIsResident());
	}

	/**
	 * Auto generated test method
	 */
	public void testgetResident() throws Exception {

		ResidentManagementServiceStub stub = new ResidentManagementServiceStub();
		GetResident getResident32 = (GetResident) getTestObject(GetResident.class);
		
		// Test 1: First try getting a non-existent resident.
		getResident32.setResidentName("Non-existent resident.");
		try {
			stub.getResident(getResident32);
			fail("resident shouldn't exist");
		} catch (NoSuchResident|AxisFault e) {}
		
		// Test 2: Now try getting an existing resident.
		getResident32.setResidentName((String) existingResident.get("name"));
		GetResidentResponse response = stub.getResident(getResident32);
		
		assertEquals((String) existingResident.get("name"), response.getResident().getName());
	}

	/**
	 * Auto generated test method
	 */
	public void testupdateRoomNumber() throws Exception {
		
		int[] roomNumbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
		int newRoomNumber = roomNumbers[randomInt(0, roomNumbers.length - 1)];

		ResidentManagementServiceStub stub = new ResidentManagementServiceStub();
		UpdateRoomNumber updateRoomNumber34 = (UpdateRoomNumber) getTestObject(UpdateRoomNumber.class);
		
		// Test 1: Update room number of an existing resident.
		updateRoomNumber34.setResidentName((String) existingResident.get("name"));
		updateRoomNumber34.setRoomNumber(newRoomNumber);
		UpdateRoomNumberResponse response = stub.updateRoomNumber(updateRoomNumber34);
		assertEquals(newRoomNumber, response.getResident().getRoomNumber());
		
		GetResident getResident = new GetResident();
		getResident.setResidentName((String) existingResident.get("name"));
		assertEquals(newRoomNumber, stub.getResident(getResident).getResident().getRoomNumber());
		
		// Test 2: Update room number of a non-existing resident.
		updateRoomNumber34.setResidentName((String) nonExistingResident.get("name"));
		updateRoomNumber34.setRoomNumber(newRoomNumber);
		try {
			stub.updateRoomNumber(updateRoomNumber34);
			fail("the room number of a non-existing resident was updated");
		} catch (NoSuchResident|AxisFault e) {}
	}

	// Create an ADBBean and provide it as the test object
	@SuppressWarnings("rawtypes")
	public org.apache.axis2.databinding.ADBBean getTestObject(
			Class type) throws Exception {
		return (org.apache.axis2.databinding.ADBBean) type.newInstance();
	}
	
	private static String randomString(int length) {
		String s = "";
		for (int i = 0; i < length; i++) {
			s += randomInt(0, 9);
		}
		return s;
	}
	
	private static int randomInt(int min, int max) {
		return min + (int)(Math.random() * ((max - min) + 1));
	}
	
	private int countResidents() throws Exception {
		ResidentManagementServiceStub stub = new ResidentManagementServiceStub();
		ResidentList list = stub.getResidents(new GetResidents()).getResidentList();
		return list.getResident() == null ? 0 : list.getResident().length;
	}
}
