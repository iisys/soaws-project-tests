
/**
 * AccountBalanceServiceTest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */
package al.elass.accountbalance;

import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.axis2.AxisFault;

import al.elass.accountbalance.AccountBalanceServiceStub.AddEntity;
import al.elass.accountbalance.AccountBalanceServiceStub.AddEntityResponse;
import al.elass.accountbalance.AccountBalanceServiceStub.AddMutation;
import al.elass.accountbalance.AccountBalanceServiceStub.AddMutationResponse;
import al.elass.accountbalance.AccountBalanceServiceStub.Balance;
import al.elass.accountbalance.AccountBalanceServiceStub.Balances;
import al.elass.accountbalance.AccountBalanceServiceStub.Entity;
import al.elass.accountbalance.AccountBalanceServiceStub.GetBalance;
import al.elass.accountbalance.AccountBalanceServiceStub.GetBalanceResponse;
import al.elass.accountbalance.AccountBalanceServiceStub.GetBalances;
import al.elass.accountbalance.AccountBalanceServiceStub.GetBalancesResponse;
import al.elass.accountbalance.AccountBalanceServiceStub.GetMutations;
import al.elass.accountbalance.AccountBalanceServiceStub.GetMutationsResponse;
import al.elass.accountbalance.AccountBalanceServiceStub.IsEntity;
import al.elass.accountbalance.AccountBalanceServiceStub.IsEntityResponse;
import al.elass.accountbalance.AccountBalanceServiceStub.MutationEntry;
import al.elass.accountbalance.AccountBalanceServiceStub.Mutations;
import al.elass.accountbalance.AccountBalanceServiceStub.MutationsRequestDetails;
import al.elass.accountbalance.AccountBalanceServiceStub.Share_type0;
import al.elass.accountbalance.AccountBalanceServiceStub.Shares_type0;
import al.elass.accountbalance.AccountBalanceServiceStub.VerifyBalances;

/*
 *  AccountBalanceServiceTest Junit test case
 */

public class AccountBalanceServiceTest extends junit.framework.TestCase {

	@SuppressWarnings("serial")
	private static Map<String, Object> existingEntity = new HashMap<String, Object>() {
		{
			try {
				put("entity", "Entity #1");
				put("balance", 9.73);
				Entity entity = new Entity();
				entity.setEntity((String) get("entity"));
				put("eEntity", entity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	@SuppressWarnings("serial")
	private static Map<String, Object> nonExistingEntity = new HashMap<String, Object>() {
		{
			try {
				put("entity", "Unknown entity #");
				put("balance", 0.00);
				Entity entity = new Entity();
				entity.setEntity((String) get("entity"));
				put("eEntity", entity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	/**
	 * Auto generated test method
	 */
	public void testgetBalance() throws Exception {

		AccountBalanceServiceStub stub = new AccountBalanceServiceStub();
		GetBalance getBalance = (GetBalance) getTestObject(GetBalance.class);
		Entity eEntity;

		// Test getBalance from existing entity.
		eEntity = (Entity) existingEntity.get("eEntity");
		getBalance.setEntity(eEntity);
		GetBalanceResponse response = stub.getBalance(getBalance);

		assertNotNull(response);
		assertEquals(eEntity.getEntity(), response.getBalance().getEntity().getEntity());

		// Test getBalance from non-existing entity.
		eEntity = (Entity) nonExistingEntity.get("eEntity");
		eEntity.setEntity(eEntity.getEntity() + randomString(6));
		getBalance.setEntity(eEntity);
		try {
			response = stub.getBalance(getBalance);
			fail("retrieved balance from a non-existing entity");
		} catch (NoSuchEntity|AxisFault e) {}
	}

	/**
	 * Auto generated test method
	 */
	public void testisEntity() throws Exception {
		AccountBalanceServiceStub stub = new AccountBalanceServiceStub();
		IsEntity isEntity = (IsEntity) getTestObject(IsEntity.class);
		IsEntityResponse response;
		
		// Test 1: check if existing entity exists.
		isEntity.setEntity((Entity) existingEntity.get("eEntity"));
		response = stub.isEntity(isEntity);
		assertTrue(response.getIsEntity());
		
		// Test 2: check if non-existing entity exists.
		isEntity.setEntity((Entity) nonExistingEntity.get("eEntity"));
		response = stub.isEntity(isEntity);
		assertFalse(response.getIsEntity());
	}

	/**
	 * Auto generated test method
	 */
	public void testaddEntity() throws Exception {

		AccountBalanceServiceStub stub = new AccountBalanceServiceStub();
		AddEntity addEntityRequest = (AddEntity) getTestObject(AddEntity.class);

		// Test 1: Add a non-existing entity with a random name.
		int countBefore = countEntities();
		Entity eEntity = new Entity();
		eEntity.setEntity((String) nonExistingEntity.get("entity") + randomString(5));
		addEntityRequest.setEntity(eEntity);
		AddEntityResponse response1 = stub.addEntity(addEntityRequest);
		// Test 1: check if the response contains the added entity.
		assertEquals(eEntity.getEntity(), response1.getEntity().getEntity());
		// Test 1: check if the amount of entities has increased by 1.
		int countAfter = countEntities();
		assertEquals(countBefore + 1, countAfter);
		// Test 1: Check if the entity was added correctly with all its values.
		GetBalance getBalance = new GetBalance();
		getBalance.setEntity(eEntity);
		GetBalanceResponse response2 = stub.getBalance(getBalance);
		assertEquals(eEntity.getEntity(), response2.getBalance().getEntity().getEntity());
		assertEquals(new Float(0.00), new Float(response2.getBalance().getValue()));

		// Test 2: Add an existing entity.
		addEntityRequest.setEntity((Entity) existingEntity.get("eEntity"));
		AddEntityResponse response3 = stub.addEntity(addEntityRequest);
		assertEquals((String) existingEntity.get("entity"), response3.getEntity().getEntity());
	}

	/**
	 * Auto generated test method
	 */
	public void testaddMutation() throws Exception {

		AccountBalanceServiceStub stub = new AccountBalanceServiceStub();
		AddMutation addMutationRequest = (AddMutation) getTestObject(AddMutation.class);
		MutationEntry mutationEntry;
		Share_type0 share;
		Shares_type0 shares;
		AddMutationResponse response;
		final int sharesCount = 4;

		// First get a few existing entities.
		GetBalancesResponse response1 = stub.getBalances(new GetBalances());
		if (response1.getBalances().getBalance().length < sharesCount) {
			for (int i = 0; i < sharesCount; i++) {
				testaddEntity();
			} // Add a few entities if we've got too few.
			response1 = stub.getBalances(new GetBalances());
		}
		Entity mutatorEntity = (Entity) existingEntity.get("eEntity");
		Balance[] entityBalance = new Balance[sharesCount];
		for (int i = 0; i < sharesCount; i++) {
			entityBalance[i] = response1.getBalances().getBalance()[i];
		}

		int mutationsCountBefore = countMutations();

		// Test 1: Add mutation while mutator doesn't exist.
		mutationEntry = new MutationEntry();
		mutationEntry.setMutatedBy((Entity) nonExistingEntity.get("eEntity"));
		mutationEntry.setDescription("descr");
		mutationEntry.setShares(new Shares_type0());
		mutationEntry.setValue(0);
		try {
			addMutationRequest.setEntry(mutationEntry);
			stub.addMutation(addMutationRequest);
			fail("mutation added with unknown mutator entry");
		} catch (NoSuchEntity|AxisFault e) {}

		// Test 2: Add mutation while one of the mutatees doesn't exist.
		mutationEntry = new MutationEntry();
		mutationEntry.setMutatedBy(mutatorEntity);
		mutationEntry.setValue(10);
		mutationEntry.setDescription("random description #" + randomString(10));
		share = new Share_type0();
		share.setEntity((Entity) nonExistingEntity.get("eEntity"));
		share.setPortion(1);
		shares = new Shares_type0();
		shares.addShare(share);
		mutationEntry.setShares(shares);
		try {
			addMutationRequest.setEntry(mutationEntry);
			stub.addMutation(addMutationRequest);
			fail("mutation added with unknown mutatee entry");
		} catch (NoSuchEntity|AxisFault e) {}

		// Test 1 and 2: check if no mutations were added.
		int mutationsCountAfter = countMutations();
		assertEquals(mutationsCountAfter, mutationsCountBefore);
		float balanceBefore = getBalance(mutatorEntity);

		// Test 3: Add mutation without shares.
		mutationsCountBefore = countMutations();
		float value = 5;
		mutationEntry = new MutationEntry();
		mutationEntry.setMutatedBy(mutatorEntity);
		mutationEntry.setValue(value);
		mutationEntry.setDescription("random description #" + randomString(10));
		shares = new Shares_type0();
		mutationEntry.setShares(shares);
		addMutationRequest.setEntry(mutationEntry);
		response = stub.addMutation(addMutationRequest);
		// Test 3: check if exactly one mutation has been added.
		assertEquals(mutationsCountBefore + 1, countMutations());
		// Test 3: check if the returned balance is the updated balance.
		assertEquals(balanceBefore + value, response.getBalance().getValue());
		// Test 3: check if the current balance is the updated balance.
		assertEquals(balanceBefore + value, getBalance(mutatorEntity));

		// Test 4: Add mutation with shares.
		mutationsCountBefore = countMutations();
		float totalValue = 10;
		mutationEntry = new MutationEntry();
		mutationEntry.setMutatedBy(mutatorEntity);
		mutationEntry.setValue(totalValue);
		mutationEntry.setDescription("random description #" + randomString(10));
		shares = new Shares_type0();
		mutationEntry.setShares(shares);
		for (int i = 0; i < sharesCount; i++) {
			share = new Share_type0();
			share.setEntity(entityBalance[i].getEntity());
			share.setPortion(1);
			shares.addShare(share);
		}
		shares.getShare()[0].setPortion(2);
		addMutationRequest.setEntry(mutationEntry);
		response = stub.addMutation(addMutationRequest);
		// Test 4: check if the correct amount of mutations have been added.
		assertEquals(mutationsCountBefore + 1 + sharesCount, countMutations());
		// Test 4: check if the updated balances are correct.
		for (int i = 0; i < sharesCount; i++) {
			float newBalance = entityBalance[i].getValue() + -1 * totalValue
					* shares.getShare()[i].getPortion() / (sharesCount + 1);
			if (shares.getShare()[i].getEntity().getEntity().equals(mutatorEntity.getEntity())) {
				newBalance += totalValue + value; // Total value of this
													// transaction plus the
													// value
													// from the previous
													// transaction.
			}
			assertEquals(newBalance, getBalance(shares.getShare()[i].getEntity()));
		}
	}

	/**
	 * Auto generated test method
	 */
	public void testgetBalances() throws Exception {
		AccountBalanceServiceStub stub = new AccountBalanceServiceStub();
		GetBalances getBalancesRequest = (GetBalances) getTestObject(GetBalances.class);
		GetBalancesResponse response = stub.getBalances(getBalancesRequest);

		assertNotNull(response);
		assertTrue(response.getBalances().getBalance().length > 0);
	}

	/**
	 * Auto generated test method
	 */
	public void testgetMutations() throws Exception {

		// Time: from one year ago to tomorrow.
		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();
		start.setTimeInMillis(new Date().getTime() - 1000 * 60 * 60 * 24 * 365);
		end.setTimeInMillis(new Date().getTime() + 1000 * 60 * 60 * 24);

		// Test 1: Get all mutations of the past year.
		Mutations mutations = getMutations(start, end, null);
		assertNotNull(mutations);
		assertTrue(mutations.getMutation().length > 0);

		// Test 2: Get all mutations of the past year of an existing entity.
		Mutations mutationsOfExistingEntity = getMutations(start, end,
				(Entity) existingEntity.get("eEntity"));
		assertNotNull(mutationsOfExistingEntity);
		assertTrue(mutationsOfExistingEntity.getMutation().length > 0);

		// Test 3: Get all mutations of the past year of a non-existing entity.
		try {
			getMutations(start, end, (Entity) nonExistingEntity.get("eEntity"));
			fail("mutations retrieved of non-existing entity");
		} catch (NoSuchEntity|AxisFault e) {}
	}

	/**
	 * Auto generated test method
	 */
	public void testverifyBalances() throws Exception {

		AccountBalanceServiceStub stub = new AccountBalanceServiceStub();
		VerifyBalances verifyBalances = (VerifyBalances) getTestObject(VerifyBalances.class);
		try {
			stub.verifyBalances(verifyBalances);
		} catch (RemoteException e) {
			fail();
		}
	}

	// Create an ADBBean and provide it as the test object
	public org.apache.axis2.databinding.ADBBean getTestObject(
			@SuppressWarnings("rawtypes") Class type) throws Exception {
		return (org.apache.axis2.databinding.ADBBean) type.newInstance();
	}

	private static String randomString(int length) {
		String s = "";
		for (int i = 0; i < length; i++) {
			s += randomInt(0, 9);
		}
		return s;
	}

	private static int randomInt(int min, int max) {
		return min + (int) (Math.random() * ((max - min) + 1));
	}

	/**
	 * Count all mutations of the past year.
	 * 
	 * @return the amount of mutations
	 * @throws Exception
	 */
	private int countMutations() throws Exception {
		// Time: from one year ago to tomorrow.
		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();
		start.setTimeInMillis(new Date().getTime() - 1000 * 60 * 60 * 24 * 365);
		end.setTimeInMillis(new Date().getTime() + 1000 * 60 * 60 * 24);

		Mutations mutations = getMutations(start, end, null);
		return mutations.getMutation() == null ? 0 : mutations.getMutation().length;
	}

	/**
	 * Assuming the entity exists, returns the balance.
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 */
	private float getBalance(Entity entity) throws Exception {
		AccountBalanceServiceStub stub = new AccountBalanceServiceStub();

		GetBalance request = new GetBalance();
		request.setEntity(entity);

		return stub.getBalance(request).getBalance().getValue();
	}

	private Mutations getMutations(Calendar start, Calendar end, Entity entity) throws Exception {

		AccountBalanceServiceStub stub = new AccountBalanceServiceStub();
		GetMutations request = (GetMutations) getTestObject(GetMutations.class);

		MutationsRequestDetails mutationsRequestDetails = new MutationsRequestDetails();
		mutationsRequestDetails.setEntity(entity);
		mutationsRequestDetails.setAfterDate(start);
		mutationsRequestDetails.setBeforeDate(end);

		request.setRequestDetails(mutationsRequestDetails);
		GetMutationsResponse response = stub.getMutations(request);
		return response.getMutations();
	}
	
	private int countEntities() throws Exception {
		AccountBalanceServiceStub stub = new AccountBalanceServiceStub();
		Balances balances = stub.getBalances(new GetBalances()).getBalances();
		return balances.getBalance() == null ? 0 : balances.getBalance().length;
	}
}
