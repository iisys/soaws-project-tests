/**
 * ConsumeStockTest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */
package al.elass.consumestock;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import al.elass.accountbalance.AccountBalanceServiceStub;
import al.elass.accountbalance.AccountBalanceServiceStub.Entity;
import al.elass.accountbalance.AccountBalanceServiceStub.GetBalance;
import al.elass.accountbalance.AccountBalanceServiceStub.GetMutations;
import al.elass.accountbalance.AccountBalanceServiceStub.Mutations;
import al.elass.accountbalance.AccountBalanceServiceStub.MutationsRequestDetails;
import al.elass.consumestock.ConsumeStockStub.ConsumeStockDetails;
import al.elass.consumestock.ConsumeStockStub.ConsumeStockResponse;
import al.elass.inventory.InventoryServiceStub;
import al.elass.inventory.InventoryServiceStub.GetInventoryItem;
import al.elass.inventory.InventoryServiceStub.Item;

/*
 *  ConsumeStockTest Junit test case
 *  
 *  Tests that should fail:
 *  - Consume stock with a non-existing resident.
 *  - Consume stock while inventory is too low.
 *  - Consume stock while resident's balance is too low.
 *  
 *  Tests that should succeed: 
 *  - Consume stock with amount zero.
 *  - Consume stock with negative amount.
 *  - Consume stock with valid and correct values.
 */

public class ConsumeStockTest extends junit.framework.TestCase {

	private static final String RESIDENT = "Resident #1";
	private static final String ITEM = "Grolsch 0.3L";
	private static final int CONSUMEAMOUNT = 2;

	/**
	 * Auto generated test method
	 */
	public void testconsumeStock() throws java.lang.Exception {
		
		ConsumeStockDetails consumeStockDetails;
		ConsumeStockResponse consumeStockResponse;

		ConsumeStockStub stub = new ConsumeStockStub();
		AccountBalanceServiceStub abStub = new AccountBalanceServiceStub();
		InventoryServiceStub iStub = new InventoryServiceStub();

		
		// First, get all current values so we can compare them later on.
		// Current balance:
		GetBalance getBalanceRequest0 = new GetBalance();
		Entity entity0 = new Entity();
		entity0.setEntity(RESIDENT);
		getBalanceRequest0.setEntity(entity0);
		final float originalBalance = abStub.getBalance(getBalanceRequest0).getBalance().getValue();
		// Current stock:
		GetInventoryItem getInventoryItemRequest0 = new GetInventoryItem();
		getInventoryItemRequest0.setItemName(ITEM);
		Item item = iStub.getInventoryItem(getInventoryItemRequest0).getItem();
		final int originalStock = item.getStock();
		final float price = item.getPrice();
		// Current amount of mutations:
		final int originalMutationsAmount = countMutations();
		
		if (originalStock < CONSUMEAMOUNT) {
			fail("Stock of " + ITEM + " is too low. Fix this before continuing.");
		}
		if (originalBalance < -100 + price*CONSUMEAMOUNT) {
			fail("Balance of " + RESIDENT + " is too low. Fix this before continuing.");
		}
		

		// Test 1: Consume stock with a non-existing resident.
		consumeStockDetails = new ConsumeStockDetails();
		consumeStockDetails.setResident("Non-existing resident.~9825256587");
		consumeStockDetails.setItem(ITEM);
		consumeStockDetails.setAmount(CONSUMEAMOUNT);
		consumeStockResponse = stub.consumeStock(consumeStockDetails);
		assertNotNull(consumeStockResponse.getError());
		assertNull(consumeStockResponse.getSuccess());
		assertTrue(consumeStockResponse.getError().contains("not found"));

		// Test 2: Consume stock while inventory is too low.
		consumeStockDetails = new ConsumeStockDetails();
		consumeStockDetails.setResident(RESIDENT);
		consumeStockDetails.setItem(ITEM);
		consumeStockDetails.setAmount(9999999 * CONSUMEAMOUNT);
		consumeStockResponse = stub.consumeStock(consumeStockDetails);
		assertNotNull(consumeStockResponse.getError());
		assertNull(consumeStockResponse.getSuccess());
		assertTrue(consumeStockResponse.getError().contains("too low"));

		// Change the current balance of a resident, so the ConsumeStock process returns a 'balance too low' error.
		try {
			PreparedStatement st = getConnection().prepareStatement(
				"UPDATE entities SET balance = -120.0 WHERE entity = ?"
			);
			st.setString(1, RESIDENT);
			st.execute();
		} catch (SQLException e) {}
		
		// Test 3: Consume stock while resident's balance is too low.
		try {
			consumeStockDetails = new ConsumeStockDetails();
			consumeStockDetails.setResident(RESIDENT);
			consumeStockDetails.setItem(ITEM);
			consumeStockDetails.setAmount(CONSUMEAMOUNT);
			consumeStockResponse = stub.consumeStock(consumeStockDetails);
			assertNotNull(consumeStockResponse.getError());
			assertNull(consumeStockResponse.getSuccess());
			assertTrue(consumeStockResponse.getError().contains("balance is too low"));
		} finally {
			// And change the current balance of the resident again, so the database is consistent.
			try {
				PreparedStatement st = getConnection().prepareStatement(
					"UPDATE entities SET balance = ? WHERE entity = ?"
				);
				st.setFloat(1, originalBalance);
				st.setString(2, RESIDENT);
				st.execute();
			} catch (SQLException e) {}
		}
		
		// Confirm that nothing has changed with the last tests.
		assertEquals(originalBalance, abStub.getBalance(getBalanceRequest0).getBalance().getValue());
		assertEquals(originalStock, iStub.getInventoryItem(getInventoryItemRequest0).getItem().getStock());
		assertEquals(originalMutationsAmount, countMutations());
		
		
		
		// Test 4: Consume stock with amount zero.
		consumeStockDetails = new ConsumeStockDetails();
		consumeStockDetails.setResident(RESIDENT);
		consumeStockDetails.setItem(ITEM);
		consumeStockDetails.setAmount(0);
		consumeStockResponse = stub.consumeStock(consumeStockDetails);
		assertNull(consumeStockResponse.getError());
		assertNotNull(consumeStockResponse.getSuccess());
		assertEquals(RESIDENT, consumeStockResponse.getSuccess().getResident().getName());
		assertEquals(originalBalance, consumeStockResponse.getSuccess().getResident().getNewBalance());
		assertEquals(ITEM, consumeStockResponse.getSuccess().getInventoryItem().getItem());
		assertEquals(price, consumeStockResponse.getSuccess().getInventoryItem().getPaidItemPrice());
		assertEquals(originalStock, consumeStockResponse.getSuccess().getInventoryItem().getNewStock());
		

		// Test 5: Consume stock with negative amount.
		consumeStockDetails = new ConsumeStockDetails();
		consumeStockDetails.setResident(RESIDENT);
		consumeStockDetails.setItem(ITEM);
		consumeStockDetails.setAmount(-1*CONSUMEAMOUNT);
		consumeStockResponse = stub.consumeStock(consumeStockDetails);
		assertNull(consumeStockResponse.getError());
		assertNotNull(consumeStockResponse.getSuccess());
		assertEquals(RESIDENT, consumeStockResponse.getSuccess().getResident().getName());
		assertEquals(originalBalance - -1*CONSUMEAMOUNT * price, consumeStockResponse.getSuccess().getResident().getNewBalance());
		assertEquals(ITEM, consumeStockResponse.getSuccess().getInventoryItem().getItem());
		assertEquals(price, consumeStockResponse.getSuccess().getInventoryItem().getPaidItemPrice());
		assertEquals(originalStock - -1*CONSUMEAMOUNT, consumeStockResponse.getSuccess().getInventoryItem().getNewStock());

		// Test 6: Consume stock with valid and correct values. The stock and balance should be as if nothing happened, because the 
		//  previous test was with a negative amount.
		consumeStockDetails = new ConsumeStockDetails();
		consumeStockDetails.setResident(RESIDENT);
		consumeStockDetails.setItem(ITEM);
		consumeStockDetails.setAmount(CONSUMEAMOUNT);
		consumeStockResponse = stub.consumeStock(consumeStockDetails);
		assertNull(consumeStockResponse.getError());
		assertNotNull(consumeStockResponse.getSuccess());
		assertEquals(RESIDENT, consumeStockResponse.getSuccess().getResident().getName());
		assertEquals(originalBalance, consumeStockResponse.getSuccess().getResident().getNewBalance());
		assertEquals(ITEM, consumeStockResponse.getSuccess().getInventoryItem().getItem());
		assertEquals(price, consumeStockResponse.getSuccess().getInventoryItem().getPaidItemPrice());
		assertEquals(originalStock, consumeStockResponse.getSuccess().getInventoryItem().getNewStock());
		
		// And finally: check the amount of new mutations.
		assertEquals(originalMutationsAmount + 3, countMutations());

	}
	
	
	
	/**
	 * Count all mutations of the past year.
	 * 
	 * @return the amount of mutations
	 * @throws Exception
	 */
	private int countMutations() throws Exception {
		
		AccountBalanceServiceStub stub = new AccountBalanceServiceStub();
		GetMutations request = new GetMutations();

		// Time: from one year ago to tomorrow.
		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();
		start.setTimeInMillis(new Date().getTime() - 1000 * 60 * 60 * 24 * 365);
		end.setTimeInMillis(new Date().getTime() + 1000 * 60 * 60 * 24);
		
		MutationsRequestDetails mutationsRequestDetails = new MutationsRequestDetails();
		mutationsRequestDetails.setEntity(null);
		mutationsRequestDetails.setAfterDate(start);
		mutationsRequestDetails.setBeforeDate(end);

		request.setRequestDetails(mutationsRequestDetails);
		
		Mutations mutations = stub.getMutations(request).getMutations();
		return mutations.getMutation() == null ? 0 : mutations.getMutation().length;
	}
	
	private final static String HOSTNAME = "127.0.0.1";
	private final static String PORT = "5432";
	private final static String USERNAME = "postgres";
	private final static String PASSWORD = "root";
	private final static String DATABASE = "soaws";
	private static Connection connection;

	private static Connection getConnection() {
		if (connection == null) {
			try {
				Class.forName("org.postgresql.Driver");
			} catch (ClassNotFoundException e) {
				System.out.println("Error loading driver: " + e);
			}

			String url = "jdbc:postgresql://" + HOSTNAME + ":" + PORT + "/" + DATABASE;

			try {
				connection = DriverManager.getConnection(url, USERNAME, PASSWORD);
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
		return connection;
	}

}
