/**
 * InventoryServiceTest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */
package al.elass.inventory;

import java.util.HashMap;
import java.util.Map;

import al.elass.inventory.InventoryServiceStub;
import al.elass.inventory.InventoryServiceStub.*;

/*
 *  InventoryServiceTest Junit test case
 */

public class InventoryServiceTest extends junit.framework.TestCase {

	@SuppressWarnings("serial")
	private static Map<String, Object> existingItem = new HashMap<String, Object>() {
		{
			try {
				put("name", "Item #1");
				put("stock", 973);
				put("price", 10/24f);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};
	
	/**
	 * Make sure the existing item is in the database.
	 * @throws Exception
	 */
	public InventoryServiceTest() throws Exception {
		InventoryServiceStub stub = new InventoryServiceStub();
		UpdateStock request;
		StockUpdate update;
		
		request = new UpdateStock();
		update = new StockUpdate();
		update.setItemName((String) existingItem.get("name"));
		update.setRelativeAmount(-50000);
		request.setStockUpdate(update);
		stub.updateStock(request);

		request = new UpdateStock();
		update = new StockUpdate();
		update.setItemName((String) existingItem.get("name"));
		update.setRelativeAmount((int) existingItem.get("stock"));
		update.setNewPrice((float) existingItem.get("price"));
		request.setStockUpdate(update);
		stub.updateStock(request);
	}

	/**
	 * Auto generated test method
	 */
	public void testgetInventoryItem() throws java.lang.Exception {

		InventoryServiceStub stub = new InventoryServiceStub();

		// Test 1: get non-existing item.
		String nonExistingItem = "Non-existing item #" + randomString(10);
		GetInventoryItem request1 = (GetInventoryItem) getTestObject(GetInventoryItem.class);
		request1.setItemName(nonExistingItem);
		GetInventoryItemResponse response1 = stub.getInventoryItem(request1);
		assertNotNull(response1);
		assertEquals(nonExistingItem, response1.getItem().getName());
		assertEquals(0,  response1.getItem().getStock());
		assertEquals(0f, response1.getItem().getPrice());
		
		// Test 2: get existing item.
		GetInventoryItem request2 = (GetInventoryItem) getTestObject(GetInventoryItem.class);
		request2.setItemName((String) existingItem.get("name"));
		GetInventoryItemResponse response2 = stub.getInventoryItem(request2);
		assertNotNull(response2);
		assertEquals((String) existingItem.get("name"), 
				response2.getItem().getName());
		assertTrue(0 <= response2.getItem().getStock());
		assertNotNull(response2.getItem().getPrice());
	}

	/**
	 * Auto generated test method
	 */
	public void testupdateStock() throws java.lang.Exception {

		InventoryServiceStub stub = new InventoryServiceStub();

		// Test 1: update stock of non-existing item to positive value.
		int inventorySizeBefore1 = countInventoryItems();
		String nonExistingItem1 = "Non-existing item #" + randomString(10);		
		UpdateStock request1 = (UpdateStock) getTestObject(UpdateStock.class);
		StockUpdate update1 = new StockUpdate();
		update1.setItemName(nonExistingItem1);
		update1.setNewPrice(0.65f);
		update1.setRelativeAmount(24);
		request1.setStockUpdate(update1);
		UpdateStockResponse response1 = stub.updateStock(request1);
		assertNotNull(response1);
		// Test 1: test if returned values are correct.
		assertEquals(nonExistingItem1, response1.getItem().getName());
		assertEquals(0.65f, response1.getItem().getPrice());
		assertEquals(24, response1.getItem().getStock());
		// Test 1: test if stock was actually added.
		GetInventoryItem request2 = (GetInventoryItem) getTestObject(GetInventoryItem.class);
		request2.setItemName(nonExistingItem1);
		Item item1 = stub.getInventoryItem(request2).getItem();
		assertEquals(nonExistingItem1, item1.getName());
		assertEquals(0.65f, item1.getPrice());
		assertEquals(24, item1.getStock());
		assertEquals(inventorySizeBefore1 + 1, countInventoryItems());

		// Test 2: update stock of non-existing item to negative value
		int inventorySizeBefore2 = countInventoryItems();
		String nonExistingItem2 = "Non-existing item #" + randomString(11);
		UpdateStock request3 = (UpdateStock) getTestObject(UpdateStock.class);
		StockUpdate update2 = new StockUpdate();
		update2.setItemName(nonExistingItem2);
		update2.setNewPrice(0.20f);
		update2.setRelativeAmount(-24);
		request3.setStockUpdate(update2);
		UpdateStockResponse response2 = stub.updateStock(request3);
		assertNotNull(response2);
		// Test 2: test if returned values are correct.
		assertEquals(nonExistingItem2, response2.getItem().getName());
		assertEquals(0.20f, response2.getItem().getPrice());
		assertEquals(0, response2.getItem().getStock());
		// Test 2: test if stock was actually added.
		GetInventoryItem request4 = (GetInventoryItem) getTestObject(GetInventoryItem.class);
		request4.setItemName(nonExistingItem2);
		Item item2 = stub.getInventoryItem(request4).getItem();
		assertEquals(nonExistingItem2, item2.getName());
		assertEquals(0.20f, item2.getPrice());
		assertEquals(0, item2.getStock());
		assertEquals(inventorySizeBefore2, countInventoryItems());
		
		// Test 3: update stock of existing item to positive value
		int amount3 = 24;
		Float price3 = 10/24f;
		int inventorySizeBefore3 = countInventoryItems();
		GetInventoryItem requestStock1 = new GetInventoryItem();
		requestStock1.setItemName((String) existingItem.get("name"));
		int itemStockBefore1 = stub.getInventoryItem(requestStock1).getItem().getStock();
		UpdateStock request5 = (UpdateStock) getTestObject(UpdateStock.class);
		StockUpdate update3 = new StockUpdate();
		update3.setItemName((String) existingItem.get("name"));
		update3.setNewPrice(price3);
		update3.setRelativeAmount(amount3);
		request5.setStockUpdate(update3);
		UpdateStockResponse response3 = stub.updateStock(request5);
		assertNotNull(response3);
		// Test 3: test if returned values are correct.
		assertEquals((String) existingItem.get("name"), response3.getItem().getName());
		assertEquals(price3, response3.getItem().getPrice());
		assertEquals(itemStockBefore1+amount3, response3.getItem().getStock());
		// Test 3: test if stock was actually added.
		GetInventoryItem request6 = (GetInventoryItem) getTestObject(GetInventoryItem.class);
		request6.setItemName((String) existingItem.get("name"));
		Item item3 = stub.getInventoryItem(request6).getItem();
		assertEquals((String) existingItem.get("name"), item3.getName());
		assertEquals(price3, item3.getPrice());
		assertEquals(itemStockBefore1+amount3, item3.getStock());
		assertEquals(inventorySizeBefore3, countInventoryItems());
		
		// Test 4: update stock of existing item to negative value (without setting the price).
		int amount4 = -2;
		int inventorySizeBefore4 = countInventoryItems();
		GetInventoryItem requestStock2 = new GetInventoryItem();
		requestStock2.setItemName((String) existingItem.get("name"));
		Item itemBefore = stub.getInventoryItem(requestStock2).getItem();
		int newStock = itemBefore.getStock() + amount4;
		UpdateStock request7 = (UpdateStock) getTestObject(UpdateStock.class);
		StockUpdate update4 = new StockUpdate();
		update4.setItemName((String) existingItem.get("name"));
		update4.setRelativeAmount(amount4);
		request7.setStockUpdate(update4);
		UpdateStockResponse response4 = stub.updateStock(request7);
		assertNotNull(response4);
		// Test 3: test if returned values are correct.
		assertEquals((String) existingItem.get("name"), response4.getItem().getName());
		assertEquals(itemBefore.getPrice(), response4.getItem().getPrice());
		assertEquals(newStock, response4.getItem().getStock());
		// Test 3: test if stock was actually added.
		GetInventoryItem request8 = (GetInventoryItem) getTestObject(GetInventoryItem.class);
		request8.setItemName((String) existingItem.get("name"));
		Item item4 = stub.getInventoryItem(request8).getItem();
		assertEquals((String) existingItem.get("name"), item4.getName());
		assertEquals(itemBefore.getPrice(), item4.getPrice());
		assertEquals(newStock, item4.getStock());
		assertEquals(inventorySizeBefore4, countInventoryItems());
		
		// Test 5: update stock of existing item to extreme negative value.
		int amount5 = -500000;
		UpdateStock request9 = (UpdateStock) getTestObject(UpdateStock.class);
		StockUpdate update5 = new StockUpdate();
		update5.setItemName((String) existingItem.get("name"));
		update5.setRelativeAmount(amount5);
		request9.setStockUpdate(update5);
		UpdateStockResponse response5 = stub.updateStock(request9);
		assertEquals(0, response5.getItem().getStock());
	}

	/**
	 * Auto generated test method
	 */
	public void testgetInventory() throws java.lang.Exception {

		InventoryServiceStub stub = new InventoryServiceStub();
		GetInventory request = (GetInventory) getTestObject(GetInventory.class);
		// Test 1: check if an ItemList is returned properly.
		GetInventoryResponse response = stub.getInventory(request);
		assertNotNull(response);
		assertNotNull(response.getItemList());
		if (response.getItemList().getItem() != null) {
			assertTrue(response.getItemList().getItem().length >= 0);

			// Test if stock level of each item is above zero.
			Item[] items = response.getItemList().getItem();
			for (int i = 0; i < items.length; i++) {
				assertTrue(items[i].getStock() > 0);
			}
		}
	}

	// Create an ADBBean and provide it as the test object
	public org.apache.axis2.databinding.ADBBean getTestObject(@SuppressWarnings("rawtypes") Class type)
			throws Exception {
		return (org.apache.axis2.databinding.ADBBean) type.newInstance();
	}

	private static String randomString(int length) {
		String s = "";
		for (int i = 0; i < length; i++) {
			s += randomInt(0, 9);
		}
		return s;
	}

	private static int randomInt(int min, int max) {
		return min + (int) (Math.random() * ((max - min) + 1));
	}
	
	private int countInventoryItems() throws Exception {
		InventoryServiceStub stub = new InventoryServiceStub();
		GetInventory request = (GetInventory) getTestObject(GetInventory.class);
		Item[] itemList = stub.getInventory(request).getItemList().getItem(); 
		return itemList == null ? 0 : itemList.length;
	}
}
