/**
 * AddStockTest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */
package al.elass.addstock;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import al.elass.accountbalance.AccountBalanceServiceStub;
import al.elass.accountbalance.AccountBalanceServiceStub.Entity;
import al.elass.accountbalance.AccountBalanceServiceStub.GetBalance;
import al.elass.accountbalance.AccountBalanceServiceStub.GetMutations;
import al.elass.accountbalance.AccountBalanceServiceStub.Mutations;
import al.elass.accountbalance.AccountBalanceServiceStub.MutationsRequestDetails;
import al.elass.addstock.AddStockStub.AddStockDetails;
import al.elass.addstock.AddStockStub.AddStockResponse;
import al.elass.inventory.InventoryServiceStub;
import al.elass.inventory.InventoryServiceStub.GetInventoryItem;

/*
 *  AddStockTest Junit test case
 *  
 *  Tests that should fail:
 *  - Add stock with a non-existing resident.
 *  - Add stock with negative amount.
 *  - Add stock with amount zero.
 *  - Add stock with negative price.
 *  
 *  Tests that should succeed:
 *  - Add stock with valid values. 
 *  - Add stock (different item) with price zero.  
 */

public class AddStockTest extends junit.framework.TestCase {
	
	private static final String RESIDENT = "Resident #1";
	private static final String ITEM = "Grolsch 0.3L";
	private static final int STOCKCHANGE = 24;
	private static final float TOTALPRICE = 9.99f;
	private static final float ITEMPRICE = TOTALPRICE / STOCKCHANGE;
	
	private static final String ITEM_FREE = "Free item!";
	private static final int STOCKCHANGE_FREEITEM = 5;
	
	/**
	 * Auto generated test method
	 */
	public void testaddStock() throws Exception {
		
		AddStockDetails addStockDetails;
		AddStockResponse addStockResponse;

		AddStockStub stub = new AddStockStub();
		AccountBalanceServiceStub abStub = new AccountBalanceServiceStub();
		InventoryServiceStub iStub = new InventoryServiceStub();
		
		// First, get all current values so we can compare them later on.
		// Current balance:
		GetBalance getBalanceRequest0 = new GetBalance();
		Entity entity0 = new Entity();
		entity0.setEntity(RESIDENT);
		getBalanceRequest0.setEntity(entity0);
		final float originalBalance = abStub.getBalance(getBalanceRequest0).getBalance().getValue();
		// Current stock:
		GetInventoryItem getInventoryItemRequest0 = new GetInventoryItem();
		getInventoryItemRequest0.setItemName(ITEM);
		final int originalStock = iStub.getInventoryItem(getInventoryItemRequest0).getItem().getStock();
		// Current amount of mutations:
		final int originalMutationsAmount = countMutations();

		
		// Test 1: Add stock with a non-existing resident
		addStockDetails = new AddStockDetails();
		addStockDetails.setResident("Non-existing resident.~135432134");
		addStockDetails.setItem(ITEM);
		addStockDetails.setAmount(STOCKCHANGE);
		addStockDetails.setTotalPrice(TOTALPRICE);
		addStockResponse = stub.addStock(addStockDetails);
		assertNotNull(addStockResponse.getError());
		assertNull(addStockResponse.getSuccess());
		assertTrue(addStockResponse.getError().contains("not found"));
		
		// Test 2: Add stock with negative amount
		addStockDetails = new AddStockDetails();
		addStockDetails.setResident(RESIDENT);
		addStockDetails.setItem(ITEM);
		addStockDetails.setAmount(-1 * STOCKCHANGE);
		addStockDetails.setTotalPrice(TOTALPRICE);
		addStockResponse = stub.addStock(addStockDetails);
		assertNotNull(addStockResponse.getError());
		assertNull(addStockResponse.getSuccess());
		assertTrue(addStockResponse.getError().contains("make sure the amount is at least 1"));
		
		// Test 3: Add stock with amount zero
		addStockDetails = new AddStockDetails();
		addStockDetails.setResident(RESIDENT);
		addStockDetails.setItem(ITEM);
		addStockDetails.setAmount(0 * STOCKCHANGE);
		addStockDetails.setTotalPrice(TOTALPRICE);
		addStockResponse = stub.addStock(addStockDetails);
		assertNotNull(addStockResponse.getError());
		assertNull(addStockResponse.getSuccess());
		assertTrue(addStockResponse.getError().contains("make sure the amount is at least 1"));
		
		// Test 4: Add stock with negative price
		addStockDetails = new AddStockDetails();
		addStockDetails.setResident(RESIDENT);
		addStockDetails.setItem(ITEM);
		addStockDetails.setAmount(STOCKCHANGE);
		addStockDetails.setTotalPrice(-1 * TOTALPRICE);
		addStockResponse = stub.addStock(addStockDetails);
		assertNotNull(addStockResponse.getError());
		assertNull(addStockResponse.getSuccess());
		assertTrue(addStockResponse.getError().contains(/*"Make sure "+*/"the total price has a non-negative value"));
		
		// Confirm that nothing has changed with the last tests.
		assertEquals(originalBalance, abStub.getBalance(getBalanceRequest0).getBalance().getValue());
		assertEquals(originalStock, iStub.getInventoryItem(getInventoryItemRequest0).getItem().getStock());
		assertEquals(originalMutationsAmount, countMutations());
		
		
		// Test 5: Add stock with valid values. 
		addStockDetails = new AddStockDetails();
		addStockDetails.setResident(RESIDENT);
		addStockDetails.setItem(ITEM);
		addStockDetails.setAmount(STOCKCHANGE);
		addStockDetails.setTotalPrice(TOTALPRICE);
		addStockResponse = stub.addStock(addStockDetails);
		assertNull(addStockResponse.getError());
		assertNotNull(addStockResponse.getSuccess());
		assertEquals(RESIDENT, addStockResponse.getSuccess().getResident().getName());
		assertEquals(originalBalance + TOTALPRICE, addStockResponse.getSuccess().getResident().getNewBalance());
		assertEquals(ITEM, addStockResponse.getSuccess().getInventoryItem().getItem());
		assertEquals(ITEMPRICE, addStockResponse.getSuccess().getInventoryItem().getNewPrice());
		assertEquals(originalStock + STOCKCHANGE, addStockResponse.getSuccess().getInventoryItem().getNewStock());
		
		// Change the current balance of a resident, so the AddStock process returns a validation error.
		try {
			PreparedStatement st = getConnection().prepareStatement(
				"UPDATE entities SET balance = balance + 1 WHERE entity = ?"
			);
			st.setString(1, RESIDENT);
			st.execute();
		} catch (SQLException e) {}
		
	    // Test 6: Add stock (different item) with price zero.
		try {
			GetInventoryItem getInventoryItemRequest1 = new GetInventoryItem();
			getInventoryItemRequest1.setItemName(ITEM_FREE);
			final int originalStock_freeitem = iStub.getInventoryItem(getInventoryItemRequest1).getItem().getStock();
			addStockDetails = new AddStockDetails();
			addStockDetails.setResident(RESIDENT);
			addStockDetails.setItem(ITEM_FREE);
			addStockDetails.setAmount(STOCKCHANGE_FREEITEM);
			addStockDetails.setTotalPrice(0);
			addStockResponse = stub.addStock(addStockDetails);
			assertNotNull(addStockResponse.getSuccess());
			assertEquals(RESIDENT, addStockResponse.getSuccess().getResident().getName());
			assertEquals(originalBalance + TOTALPRICE + 1, addStockResponse.getSuccess().getResident().getNewBalance());
			assertEquals(ITEM_FREE, addStockResponse.getSuccess().getInventoryItem().getItem());
			assertEquals(0f, addStockResponse.getSuccess().getInventoryItem().getNewPrice());
			assertEquals(originalStock_freeitem + STOCKCHANGE_FREEITEM, addStockResponse.getSuccess().getInventoryItem().getNewStock());
			assertNotNull(addStockResponse.getError());
			assertTrue(addStockResponse.getError().contains("balance consistency verification failed"));
		
		} finally {
			// And change the current balance of the resident again, so the database is consistent.
			try {
				PreparedStatement st = getConnection().prepareStatement(
					"UPDATE entities SET balance = balance - 1 WHERE entity = ?"
				);
				st.setString(1, RESIDENT);
				st.execute();
			} catch (SQLException e) {}
		}
	}
	
	/**
	 * Count all mutations of the past year.
	 * 
	 * @return the amount of mutations
	 * @throws Exception
	 */
	private int countMutations() throws Exception {
		
		AccountBalanceServiceStub stub = new AccountBalanceServiceStub();
		GetMutations request = new GetMutations();

		// Time: from one year ago to tomorrow.
		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();
		start.setTimeInMillis(new Date().getTime() - 1000 * 60 * 60 * 24 * 365);
		end.setTimeInMillis(new Date().getTime() + 1000 * 60 * 60 * 24);
		
		MutationsRequestDetails mutationsRequestDetails = new MutationsRequestDetails();
		mutationsRequestDetails.setEntity(null);
		mutationsRequestDetails.setAfterDate(start);
		mutationsRequestDetails.setBeforeDate(end);

		request.setRequestDetails(mutationsRequestDetails);
		
		Mutations mutations = stub.getMutations(request).getMutations();
		return mutations.getMutation() == null ? 0 : mutations.getMutation().length;
	}
	
	private final static String HOSTNAME = "127.0.0.1";
	private final static String PORT = "5432";
	private final static String USERNAME = "postgres";
	private final static String PASSWORD = "root";
	private final static String DATABASE = "soaws";
	private static Connection connection;

	private static Connection getConnection() {
		if (connection == null) {
			try {
				Class.forName("org.postgresql.Driver");
			} catch (ClassNotFoundException e) {
				System.out.println("Error loading driver: " + e);
			}

			String url = "jdbc:postgresql://" + HOSTNAME + ":" + PORT + "/" + DATABASE;

			try {
				connection = DriverManager.getConnection(url, USERNAME, PASSWORD);
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
		return connection;
	}
}
