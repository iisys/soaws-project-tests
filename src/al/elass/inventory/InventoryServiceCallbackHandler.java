
/**
 * InventoryServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */

    package al.elass.inventory;

    /**
     *  InventoryServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class InventoryServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public InventoryServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public InventoryServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getInventory method
            * override this method for handling normal response from getInventory operation
            */
           public void receiveResultgetInventory(
                    al.elass.inventory.InventoryServiceStub.GetInventoryResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getInventory operation
           */
            public void receiveErrorgetInventory(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for updateStock method
            * override this method for handling normal response from updateStock operation
            */
           public void receiveResultupdateStock(
                    al.elass.inventory.InventoryServiceStub.UpdateStockResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateStock operation
           */
            public void receiveErrorupdateStock(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getInventoryItem method
            * override this method for handling normal response from getInventoryItem operation
            */
           public void receiveResultgetInventoryItem(
                    al.elass.inventory.InventoryServiceStub.GetInventoryItemResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getInventoryItem operation
           */
            public void receiveErrorgetInventoryItem(java.lang.Exception e) {
            }
                


    }
    