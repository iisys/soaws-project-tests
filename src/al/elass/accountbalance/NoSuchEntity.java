
/**
 * NoSuchEntity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */

package al.elass.accountbalance;

public class NoSuchEntity extends java.lang.Exception{

    private static final long serialVersionUID = 1460282314627L;
    
    private al.elass.accountbalance.AccountBalanceServiceStub.NoSuchEntityFault faultMessage;

    
        public NoSuchEntity() {
            super("NoSuchEntity");
        }

        public NoSuchEntity(java.lang.String s) {
           super(s);
        }

        public NoSuchEntity(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public NoSuchEntity(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(al.elass.accountbalance.AccountBalanceServiceStub.NoSuchEntityFault msg){
       faultMessage = msg;
    }
    
    public al.elass.accountbalance.AccountBalanceServiceStub.NoSuchEntityFault getFaultMessage(){
       return faultMessage;
    }
}
    