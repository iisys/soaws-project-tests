
/**
 * AccountBalanceServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */

    package al.elass.accountbalance;

    /**
     *  AccountBalanceServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class AccountBalanceServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public AccountBalanceServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public AccountBalanceServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for addEntity method
            * override this method for handling normal response from addEntity operation
            */
           public void receiveResultaddEntity(
                    al.elass.accountbalance.AccountBalanceServiceStub.AddEntityResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from addEntity operation
           */
            public void receiveErroraddEntity(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getBalance method
            * override this method for handling normal response from getBalance operation
            */
           public void receiveResultgetBalance(
                    al.elass.accountbalance.AccountBalanceServiceStub.GetBalanceResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBalance operation
           */
            public void receiveErrorgetBalance(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getMutations method
            * override this method for handling normal response from getMutations operation
            */
           public void receiveResultgetMutations(
                    al.elass.accountbalance.AccountBalanceServiceStub.GetMutationsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getMutations operation
           */
            public void receiveErrorgetMutations(java.lang.Exception e) {
            }
                
               // No methods generated for meps other than in-out
                
           /**
            * auto generated Axis2 call back method for getBalances method
            * override this method for handling normal response from getBalances operation
            */
           public void receiveResultgetBalances(
                    al.elass.accountbalance.AccountBalanceServiceStub.GetBalancesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBalances operation
           */
            public void receiveErrorgetBalances(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for addMutation method
            * override this method for handling normal response from addMutation operation
            */
           public void receiveResultaddMutation(
                    al.elass.accountbalance.AccountBalanceServiceStub.AddMutationResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from addMutation operation
           */
            public void receiveErroraddMutation(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for isEntity method
            * override this method for handling normal response from isEntity operation
            */
           public void receiveResultisEntity(
                    al.elass.accountbalance.AccountBalanceServiceStub.IsEntityResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from isEntity operation
           */
            public void receiveErrorisEntity(java.lang.Exception e) {
            }
                


    }
    