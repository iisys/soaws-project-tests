
/**
 * ResidentManagementServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */

    package al.elass.residentmanagement;

    /**
     *  ResidentManagementServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class ResidentManagementServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public ResidentManagementServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public ResidentManagementServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getResidents method
            * override this method for handling normal response from getResidents operation
            */
           public void receiveResultgetResidents(
                    al.elass.residentmanagement.ResidentManagementServiceStub.GetResidentsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getResidents operation
           */
            public void receiveErrorgetResidents(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for updateInfo method
            * override this method for handling normal response from updateInfo operation
            */
           public void receiveResultupdateInfo(
                    al.elass.residentmanagement.ResidentManagementServiceStub.UpdateInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateInfo operation
           */
            public void receiveErrorupdateInfo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for moveOutResident method
            * override this method for handling normal response from moveOutResident operation
            */
           public void receiveResultmoveOutResident(
                    al.elass.residentmanagement.ResidentManagementServiceStub.MoveOutResidentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from moveOutResident operation
           */
            public void receiveErrormoveOutResident(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for addResident method
            * override this method for handling normal response from addResident operation
            */
           public void receiveResultaddResident(
                    al.elass.residentmanagement.ResidentManagementServiceStub.AddResidentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from addResident operation
           */
            public void receiveErroraddResident(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getResident method
            * override this method for handling normal response from getResident operation
            */
           public void receiveResultgetResident(
                    al.elass.residentmanagement.ResidentManagementServiceStub.GetResidentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getResident operation
           */
            public void receiveErrorgetResident(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for updateRoomNumber method
            * override this method for handling normal response from updateRoomNumber operation
            */
           public void receiveResultupdateRoomNumber(
                    al.elass.residentmanagement.ResidentManagementServiceStub.UpdateRoomNumberResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateRoomNumber operation
           */
            public void receiveErrorupdateRoomNumber(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for isResident method
            * override this method for handling normal response from isResident operation
            */
           public void receiveResultisResident(
                    al.elass.residentmanagement.ResidentManagementServiceStub.IsResidentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from isResident operation
           */
            public void receiveErrorisResident(java.lang.Exception e) {
            }
                


    }
    